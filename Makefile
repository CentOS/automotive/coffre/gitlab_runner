.PHONY: init plan

export TF_VAR_access_key := $(shell aws configure get aws_access_key_id --profile upstream-stage)
export TF_VAR_secret_key := $(shell aws configure get aws_secret_access_key --profile upstream-stage)

ifneq (,$(wildcard ./.env))
    include .env
    export
endif

init:
	terraform -chdir=iac init

plan:
	terraform -chdir=iac plan