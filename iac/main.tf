terraform {
  # backend "http" {
  #   skip_cert_verification = true
  # }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.48.0"
    }
  }
}

provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

module "gitlab-runner" {
  source  = "npalm/gitlab-runner/aws"
  version = "5.6.0"

  aws_region  = var.region
  environment = "spot-runners"

  runners_name             = var.runners_name
  runners_gitlab_url       = var.gitlab_url
  enable_runner_ssm_access = true

  vpc_id                        = var.vpc_id
  subnet_id                     = var.subnet_id
  metrics_autoscaling           = ["GroupDesiredCapacity", "GroupInServiceCapacity"]
  docker_machine_spot_price_bid = "on-demand-price"

  gitlab_runner_registration_config = {
    registration_token = var.runner_token
    tag_list           = var.tag_list
    description        = var.description
    locked_to_project  = var.locked_to_project
    run_untagged       = var.run_untagged
    maximum_timeout    = var.maximum_timeout
  }
}
