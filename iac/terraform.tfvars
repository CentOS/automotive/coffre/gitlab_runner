region = "eu-west-2"
# access_key        = ""
# secret_key        = ""
environment  = "spot-runners"
runners_name = "lmilbaum"
gitlab_url   = "https://gitlab.com/"
# runner_token      = ""
tag_list          = "lmilbaum"
description       = "runner default"
locked_to_project = "true"
run_untagged      = "false"
maximum_timeout   = "3600"
vpc_id            = "vpc-b5c2e9dd"
subnet_id         = "subnet-cd890681"
ssh_public_key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDU16sWO6O5xfztZVZ7K6E9YXDzqCyZu9Hn2oX3JzsJVlH8kghC1+oswPVh4/+/8mk6pDyTdVf8g5DbJbhjE2HHEHY3QTNFEF61bG/R48DqsOzOfwZh3gFDjUVvP17e1en5oawqxV5gAIDSTeqZwaHFKlT0qcAqBxrSDJoUl3MFwmvOzSt0qP+D5vlf6hVFNyLMds9cpBmwot2EH1AcsMWaqdMFjKPiEPYydd+0A8H4gt34QB9XL6/UIa7sMAPN3P+Z4mFa17X/hQDzt3C+lD3FysJ4YGoO32ctZBOUM7n90cuiUcdp6klDJSv4iRNP8WMhgvhyaCA8EdrOxPbRMfjj lmilbaum@lmilbaum.tlv.csb"
